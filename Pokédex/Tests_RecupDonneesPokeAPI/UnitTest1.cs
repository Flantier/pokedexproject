using System;
using System.Threading.Tasks;
using Xunit;

namespace Tests_Pokedex
{
    public class UnitTest1
    {
        [Fact]
        public async Task TestConnexionBadUrlEvol()
        //lors de la tentative de r�cup�ration d'une cha�ne d'�volution, pour tout probl�me d'Url le programme doit renvoyer le string "Err" pour annoncer le probl�me
        {
            Assert.Equal("Err", await RecupDonneesPokeAPI.RecupDonnees.RecupPokemonEvolAsync("Le pif"));
        }

        [Fact]
        public async Task TestConnexionBadUrlPokemonSpec()
        //lors de la tentative de r�cup�ration des donn�es avanc�es d'un Pok�mon, pour tout probl�me d'Url le programme doit renvoyer le string "Err" pour annoncer le probl�me
        {
            Assert.Equal("Err", await RecupDonneesPokeAPI.RecupDonnees.RecupPokemonSpecAsync(-1));
        }
        
        
        [Fact]
        public void TestMauvaisNomPokemon()
        //Voir si un mauvais nom de Pok�mon ou index sont bien g�r�s
        {
            ModeleDonnees.StockageDonneesPokemon random = new ModeleDonnees.StockageDonneesPokemon();

            Assert.False(random.AffichageDetailDonnees("bulbizarre")); //Doit �tre en anglais donc n'existe pas
            Assert.False(random.AffichageDetailDonnees("-1")); //Out of index
            Assert.False(random.AffichageDetailDonnees("1119"));//Idem
            Assert.True(random.AffichageDetailDonnees("bulbasaur")); //Existe
            Assert.True(random.AffichageDetailDonnees("1")); //Existe
            Assert.True(random.AffichageDetailDonnees("Ditto")); //Enclenche une exeption car pas d'�volution mais renvoie true quand m�me car existe bel et bien
        }

        
    }
}
