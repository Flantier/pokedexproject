﻿using PokeApiNet;
using System.Net.Http;
using System.Threading.Tasks;
using Pokedex = RecupDonneesPokeAPI.ClassesPourPokedex.Pokedex;

namespace RecupDonneesPokeAPI
{
    public static class RecupDonnees
    {

        //Pokédex

        public static async Task<string> RecupPokedexAsync(int aPartirDe, int combien)
        /*récupère une partie du pokédex (format JSON), débutant à partir d'un nombre entré en paramètre.
        */
        {
            using (var v_HttpClient = new HttpClient())
            {
                HttpResponseMessage reponsePokeAPI;
                try
                {
                    reponsePokeAPI = await v_HttpClient.GetAsync($"https://pokeapi.co/api/v2/pokemon?limit={combien}&offset={aPartirDe}");
                    reponsePokeAPI.EnsureSuccessStatusCode();
                    string reponsePokedexString = await reponsePokeAPI.Content.ReadAsStringAsync();
                    return reponsePokedexString;
                }
                catch
                {
                    return "Err";
                }

            }
        }

        public static Pokedex PokedexJSONVersClasse(string pokedexJson)
        /*Transforme le Json d'une partie du pokedex reçu en instance de classe pokedex pour accéder plus facilement aux données
        */
        {
            Pokedex pokedex = new Pokedex();
            //gerer si pokedexJson = err
            pokedex = Newtonsoft.Json.JsonConvert.DeserializeObject<Pokedex>(pokedexJson);
            return pokedex;
        }





        //Pokémon : infos de base

        public static async Task<Pokemon> RecupPokemonAsync(string nom, PokeApiClient pokeClient)
        /*Récupère un Pokémon (classe issue de l'API PokeAPINet) via son nom s'il existe
        */
        {
            pokeClient = new PokeApiClient();
            Pokemon pokemon = await pokeClient.GetResourceAsync<Pokemon>(nom);
            pokeClient.Dispose();
            return pokemon;
        }




        //Pokémon : chaîne d'évolution

        public static async Task<string> RecupPokemonEvolAsync(string url)
        /*Récupère un l'arbre d'évolution d'un pokémon via un url s'il existe
        */
        {
            using (var v_HttpClient = new HttpClient())
            {
                HttpResponseMessage reponsePokeAPI;
                try
                {
                    reponsePokeAPI = await v_HttpClient.GetAsync(url);
                    reponsePokeAPI.EnsureSuccessStatusCode();
                    string reponsePokedexString = await reponsePokeAPI.Content.ReadAsStringAsync();
                    return reponsePokedexString;
                }
                catch
                {
                    return "Err";
                }

            }
        }

        public static EvolutionChain PokemonEvolJSONVersClasse(string pokemonEvolJson)
        /*Transforme le Json d'une chaine d'évolution d'un pokémon en Classe issue de l'api Pokemon
        */
        {
            EvolutionChain pokemonEvol = new EvolutionChain();
            //gerer si pokedexJson = err
            pokemonEvol = Newtonsoft.Json.JsonConvert.DeserializeObject<EvolutionChain>(pokemonEvolJson);
            return pokemonEvol;
        }




        //Pokémon : informations avancées

        public static async Task<string> RecupPokemonSpecAsync(int id)
        /*Récupère un les infos d'un pokémon via son id s'il existe
        */
        {
            using (var v_HttpClient = new HttpClient())
            {
                HttpResponseMessage reponsePokeAPI;
                try
                {
                    reponsePokeAPI = await v_HttpClient.GetAsync($"https://pokeapi.co/api/v2/pokemon-species/{id}/");
                    reponsePokeAPI.EnsureSuccessStatusCode();
                    string reponsePokedexString = await reponsePokeAPI.Content.ReadAsStringAsync();
                    return reponsePokedexString;
                }
                catch
                {
                    return "Err";
                }

            }
        }

        public static PokemonSpecies PokemonSpecJSONVersClasse(string pokemonSpecJson)
        /*Transforme le Json d'infos d'un pokémon en Classe issue de l'api Pokemon
        */
        {
            PokemonSpecies pokemonSpec = new PokemonSpecies();
            //gerer si pokedexJson = err
            pokemonSpec = Newtonsoft.Json.JsonConvert.DeserializeObject<PokemonSpecies>(pokemonSpecJson);
            return pokemonSpec;
        }



    }
}
