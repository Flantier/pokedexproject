﻿using System.Text.Json.Serialization;

namespace RecupDonneesPokeAPI.ClassesPourPokedex
{
    public class Pokedex
    {
        [JsonPropertyName("count")]
        public int count { get; set; }
        [JsonPropertyName("next")]
        public string next { get; set; }
        [JsonPropertyName("previous")]
        public PokemonForPokedex[] results { get; set; }

        public PokemonForPokedex[] pokedex { get; set; }
    }
}
