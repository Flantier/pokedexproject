﻿using System.Text.Json.Serialization;

namespace RecupDonneesPokeAPI.ClassesPourPokedex
{
    public class PokemonForPokedex
    {
        [JsonPropertyName("name")]
        public string name { get; set; }
        [JsonPropertyName("url")]
        public string url { get; set; }

    }
}
