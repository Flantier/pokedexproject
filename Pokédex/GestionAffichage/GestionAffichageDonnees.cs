﻿using ModeleDonnees;
using System;

namespace GestionAffichage
{
    public static class GestionAffichageDonnees
    {
        //il suffirait de changer le classe "StockageDonneesPokemon" par une autre classe héritant de "AffichageConsole" pour pouvoir afficher à guise d'autres types de données
        public static StockageDonneesPokemon donneesPokemon; //classe qui permettra de stocker les données sur le pokedex et les pokémon afin de pouvoir ensuite les afficher
        public static bool fin = false; //permet de sortir de la boucle de "MenuPrincipal" si l'utilisateur souhaite arrêter (voir "FinAppli")
        public static bool principal = true; //permet de savoir si la vue actuelle est sur le pokédex (true) ou sur un pokémon en spécifique (false)
        public static string nom; //permet de stocker temporairement le nom du pokémon actuellement vu (évite certains problèmes d'affichage)

        public static void MenuPrincipal()
        /*procédure mère qui s'occupera du déroulement de l'affichage en fonction des chois de l'utilisateur.
        */
        {
            //démarrage
            donneesPokemon = new StockageDonneesPokemon();

            while (!fin)
            {
                Console.Write("\nQue voulez-vous faire ? (Tapez ! pour connaitre l'effet des commandes) \n(p | s | n | q | r ) : ");
                ReponseUtilisateur(Console.ReadLine());
            }

            donneesPokemon.AffichagePhrases(1);
            Console.ReadKey();
        }

        public static void ReponseUtilisateur(string reponse)
        /*En fonction de ce que l'utilisateur veut voir, cette procédure répondra en redirigeant le code vers les bonnes fonctions associées à la demande.
         * reponse : la lettre que l'utilisateur aura entré.
        */
        {
            Console.Clear();
            if (reponse == "p") { ModificationVueDonnees(-1); }
            else if (reponse == "s") { ModificationVueDonnees(1); }
            else if (reponse == "q") { FinAppli(); }
            else if (reponse == "r") { Retour(); }
            else if (reponse == "!") { AideCommandes(); }
            else if (reponse == "n") { RechercheChamps(); }
            else { Console.WriteLine("Commandes incorrecte - veuillez réessayer"); }
        }

        public static void FinAppli()
        /*Lorsque l'utilisateur indique "q" comme choix. Gère la sortie de la procédure "MenuPrincipal" si le joueur souhaite réellement quitter.
        * resultat : choix de l'utilisateur quant à la sortie du programme.
        */
        {
            Console.WriteLine("Voulez-vous quitter l'application ? o|n : ");
            string resultat = Console.ReadLine();
            if (resultat == "o")  fin = true;
            else
            {
                Console.Clear();
                donneesPokemon.AffichageDonneesGrossier(0);
                principal = true;
            }
        }

        public static void AideCommandes()
        /* Affiche l'interêt de chaque commande à l'utilisateur.
        */
        {
            if (!principal) { bool result = donneesPokemon.AffichageDetailDonnees(nom); }
            else donneesPokemon.AffichageDonneesGrossier(0);

            Console.WriteLine("\np : retour en arrière (si possible) de 10 champs de données."
                + "\ns : avance (si possible) de 10 champs de données."
                + "\nq : quitter l'application."
                + "\nr : retour arrière (sert  après un affichage détaillé d'un des champs, afin de revenir au menu principal)."
                + "\nn : permet de pouvoir rentrer le nom d'un champs que l'on voudrait voir plus en détail."
                + "\n! : affiche la liste des commandes.");
        }

        public static void Retour()
        /*Lorsque la vue est sur un Pokémon, permet de revenir à une vue du Pokédex. Si utilisé sur une vue Pokédex.
        */
        {
            if (!principal)
            {
                donneesPokemon.AffichageDonneesGrossier(0);
                principal = true;

            }
            else //si l'utilisateur est déjà au menu principal
            { 
                Console.WriteLine("Commande incorrecte - vous êtes déjà au menu principal et ne pouvez pas revenir plus en arrière.");
                donneesPokemon.AffichageDonneesGrossier(0);

            }
        }

        public static void ModificationVueDonnees(int avance)
        /*Si l'utilisateur est sur la vue Pokédex, permet de passer à la page suivante ou précédente.
        */
        {
            if (!principal) { //Si on est sur une vue Pokémon, on affiche un message d'erreur
                Console.WriteLine("Pour utiliser 'p' ou 's', il faut se situer au menu principal.");

                bool result = donneesPokemon.AffichageDetailDonnees(nom);
            }
            else { donneesPokemon.AffichageDonneesGrossier(avance); }
        }

        public static void RechercheChamps()
        /*Actionne le mécanisme de recherche d'un pokémon en demandant d'indiquer le pokémon par son nom ou id
         * nom : nom ou id du pokémon recherché
        */
        {
            Console.Write("Quel est le nom du pokémon que vous souhaitez voir ? (Vous pouvez entrer le nom du pokémon en anglais ou son indice dans le pokédex) : ");
            nom = Console.ReadLine();
            Console.Clear();
            bool result = donneesPokemon.AffichageDetailDonnees(nom);
            if (result) { principal = false; }
            else //s'il y a eu une erreur quelconque avec l'affichage de pokémon (mauvais nom ou problèmes avec le site par ex)
            {
                donneesPokemon.AffichageDonneesGrossier(0);
                principal = true;
            }
        }


    }
}
