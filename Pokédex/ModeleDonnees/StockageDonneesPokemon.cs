﻿using PokeApiNet;
using RecupDonneesPokeAPI;
using System;
using Pokedex = RecupDonneesPokeAPI.ClassesPourPokedex.Pokedex;

namespace ModeleDonnees
{
    public class StockageDonneesPokemon : StockageDonnees
    {
        public Pokedex pokedex { get; set; }
        public int compteActuel = 0;
        public int pas = 10;
        public PokeApiClient pokeClient = new PokeApiClient();

        public StockageDonneesPokemon()
        {
            AffichagePhrases(0);
            AffichageDonneesGrossier(0); //Affichage initial

        }

        public override bool AffichageDetailDonnees(string nom)
        /*Permet d'afficher en détail un pokémon (nom, id, type(s), description, chaîne d'évolution.
         * evol : 2 types d'erreurs sont possible dans le try/catch. Permet de connaitre à laquelle nous avons affaire lors d'un potentiel catch
         * pokemon : les données de base du pokémon recherché
         * pokeSpec : les données avancées du pokémon recherché
         * evolutionPokemon : données relatives à la chaîne d'évolution du pokémon recherché
        */
        {
            bool evol = false;
            try
            {
                //nom
                Pokemon pokemon = RecupDonnees.RecupPokemonAsync(nom, pokeClient).Result;
                //type(s)
                Console.Write($"Nom du Pokémon : {pokemon.Name} (N°{pokemon.Id})\nType(s) : ");
                foreach (PokemonType type in pokemon.Types)
                { Console.Write(type.Type.Name + " "); }
                //description
                PokemonSpecies pokeSpec = RecupDonnees.PokemonSpecJSONVersClasse(RecupDonnees.RecupPokemonSpecAsync(pokemon.Id).Result);
                Console.WriteLine($"\nDescription : {pokeSpec.FlavorTextEntries[0].FlavorText.Replace("\f", "\n")}");
                //chaîne d'évolution 
                EvolutionChain evolutionPokemon = RecupDonnees.PokemonEvolJSONVersClasse(RecupDonnees.RecupPokemonEvolAsync(pokeSpec.EvolutionChain.Url).Result);
                Console.Write($"\nChaîne d'évolution : {evolutionPokemon.Chain.Species.Name}");
                evol = true;
                if (evolutionPokemon.Chain.EvolvesTo[0].Species.Name != null)
                {
                    Console.Write(" --> ");
                    for (int compteur = 0; compteur < evolutionPokemon.Chain.EvolvesTo.Count; compteur++)
                    {
                        if (compteur != 0) Console.Write(" | ");
                        Console.Write(evolutionPokemon.Chain.EvolvesTo[compteur].Species.Name);
                    }
                    //if (evolutionPokemon.Chain.EvolvesTo[0].EvolvesTo[0].Species.Name != null) Console.Write("-> " + evolutionPokemon.Chain.EvolvesTo[0].EvolvesTo[0].Species.Name);
                    if (evolutionPokemon.Chain.EvolvesTo[0].EvolvesTo[0].Species.Name != null)
                    {
                        Console.Write(" --> " + evolutionPokemon.Chain.EvolvesTo[0].EvolvesTo[0].Species.Name);
                        for (int compteur = 1; compteur< evolutionPokemon.Chain.EvolvesTo[0].EvolvesTo.Count;compteur++)
                        {
                            Console.Write(" | " + evolutionPokemon.Chain.EvolvesTo[0].EvolvesTo[compteur].Species.Name);
                        }
                    }
                }
                Console.WriteLine();

                return true;
            }
            catch
            {
                if (!evol)
                {
                    Console.WriteLine("Nom de pokémon incorrecte ou non reconnu dans le pokédex.\n");
                    return false;
                }
                else
                {
                    Console.WriteLine();
                    return true;
                }
            }


        }

        public override void AffichageDonneesGrossier(int avance)
        /* Affichera à l'écran un certain nombre des champs du pokédex.
         * avance : permet de savoir si l'on doit avancer, reculer ou rester sur la même vision des champs actuels du pokédex.
         * pas : pas indiquant le nombre de champs que l'on peut voir du Pokédex à chaque affichage
         * pokedex : objet contenant les 10 prochains champs du pokédex à afficher en fonction du choix de l'utilisateur
        */
        {

            if (avance == 1) { compteActuel += pas; }
            else if (avance == -1) { compteActuel -= pas; if (compteActuel < 0) compteActuel = 0; }


            pokedex = RecupDonnees.PokedexJSONVersClasse(RecupDonnees.RecupPokedexAsync(compteActuel, pas).Result);
            Console.WriteLine($"Liste des pokémon de {compteActuel} à {compteActuel + pokedex.results.Length} :");
            if (compteActuel >= pokedex.count) compteActuel = pokedex.count - 10; //permet de ne pas dépasser les limites
            foreach (RecupDonneesPokeAPI.ClassesPourPokedex.PokemonForPokedex pokemon in pokedex.results)
            {
                Console.WriteLine($"-{pokemon.name}");
            }
        }

        public override void AffichagePhrases(int choix)
        /* Permet de choisir des phrases plus personnelles à une application pokédex malgré un code générique d'affichage de données.
        */
        {
            if (choix == 0) Console.WriteLine("Bienvenue sur l'application du Pokédex National !\n");
            else if (choix == 1) Console.WriteLine("Merci d'avoir utilisé notre Pokédex !");
        }
    }
}
