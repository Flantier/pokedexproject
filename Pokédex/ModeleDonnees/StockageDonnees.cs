﻿namespace ModeleDonnees
{
    public abstract class StockageDonnees
    {
        public abstract void AffichageDonneesGrossier(int avance);
        public abstract bool AffichageDetailDonnees(string nom);

        public abstract void AffichagePhrases(int choix);
    }
}
